'use strict';

/**
 * @ngdoc overview
 * @name highchartsApp
 * @description
 * # highchartsApp
 *
 * Main module of the application.
 */
angular
  .module('highchartsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/about.html',
        controller: 'ChartsCtrl',
        controllerAs: 'about'
      })

      .otherwise({
        redirectTo: '/'
      });
  });
