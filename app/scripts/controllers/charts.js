'use strict';

/**
 * @ngdoc function
 * @name highchartsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the highchartsApp
 */
angular.module('highchartsApp')
  .controller('ChartsCtrl', function ($scope) {

    $scope.charts = [];
    for(var i=0;i<10;i++) {
      $scope.charts.push(i);
    }
  });
