angular.module('highchartsApp').controller('ChartController',function($scope,$log,$q) {
  function ChartApi(chart) {
    this.chart = chart;
    var series = {};
    this.addSeries = function (config) {
      if(angular.isUndefined(series[config.key])) {
        series[config.key] = this.chart.addSeries(config);
      };
    };
    this.addDataItem=function(key,data) {
      if(angular.isDefined(key)) {
        series[key].addPoint(data,false);
      }
    };
    this.redraw = function () {
      this.chart.redraw();
    };
  };
  $scope.chartDefer = $q.defer();
  $scope.chartDefer.promise.then(function(chart) {
    $scope.api = new ChartApi(chart);
  });
});
