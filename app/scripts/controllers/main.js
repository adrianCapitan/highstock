'use strict';

/**
 * @ngdoc function
 * @name highchartsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the highchartsApp
 */
angular.module('highchartsApp')
  .controller('MainCtrl', function ($scope,$timeout,$interval) {
    $scope.chartConfig = {
      chart:{
        type:'areaspline',
        height: 700
      }
    };
    function Test(chartApi) {
      this.chartApi = chartApi;

      function addSeries() {
        for(var i=0;i<10;i++) {
          chartApi.addSeries({
            key:'series'+i,
            data:[]
          });
        }
      }
      function randomPoint() {
        return Math.floor(Math.random()*1000+1);
      };
      function loadPoints() {
        for(var i=0;i<10;i++) {
          chartApi.addDataItem('series'+i,[Date.now(),randomPoint()]);
        }
      };
      function startInterval() {
        setInterval(function () {
          loadPoints();
        },500);
      };

      this.startTest = function () {
        addSeries();
        startInterval();
      };
    };
    setInterval(function () {
      $scope.chartApi.redraw();
    },2000);
    $timeout(function () {
      var test = new Test($scope.chartApi);
      test.startTest();
    },1000);
  });
