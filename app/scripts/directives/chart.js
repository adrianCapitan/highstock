'use strict';

angular.module('highchartsApp')
  .directive('chart', function ($log) {
    return {
      controller: 'ChartController',
      scope: {
        chart: '&',
        api:'='
      },
      template:'<div class="inner-chart"></div>',
      link: function (scope,element) {
        var config = scope.chart();
        config.chart.renderTo = element[0];
        config.chart.events = {
          load: function () {
            scope.chartDefer.resolve(this);
          }
        };
        new Highcharts.StockChart(config);
      }
    };
  });
