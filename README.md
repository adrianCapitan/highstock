# highcharts

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Build & development

Git clone  
Install node.js  
Commands  
`npm install -g bower`  
`npm install -g grunt-cli`  
`bower install`  
`npm install`  
For preview: **grunt serve**  

## Testing

Running `grunt test` will run the unit tests with karma.